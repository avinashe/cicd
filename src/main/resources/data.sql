DROP TABLE Person IF EXISTS;

CREATE TABLE Person
(
    pId BIGINT IDENTITY NOT NULL PRIMARY KEY,
    fName varchar2 (20),
    lName varchar2 (20)
);