package com.amdocs.batch.simple.writer;

import com.amdocs.batch.simple.model.Person;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class PersonItemWriter {

    private static final String INSERT_QUERY = "INSERT INTO Person (fName,lName) values(:fName,:lName)";

    private DataSource dataSource;

    public PersonItemWriter(@Qualifier("testDataSource") DataSource testDataSource) {
        this.dataSource = testDataSource;
    }

    @Bean
    public JdbcBatchItemWriter<Person> writer(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Person>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<Person>())
                .sql(INSERT_QUERY)
                .dataSource(this.dataSource)
                .dataSource(dataSource)
                .build();
    }
}
