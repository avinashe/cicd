package com.amdocs.batch.simple.processor;

import com.amdocs.batch.simple.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class PersonItemProcessor implements ItemProcessor<Person, Person> {
    @Override
    public Person process(Person person) throws Exception {

        log.debug("Data Recevied from reader   " + person.toString());

        return new Person()
                .builder()
                .fName(person.getFName().toUpperCase())
                .lName(person.getLName().toUpperCase())
                .build();
    }
}
