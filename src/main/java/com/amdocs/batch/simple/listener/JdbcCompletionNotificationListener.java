package com.amdocs.batch.simple.listener;

import com.amdocs.batch.simple.model.Person;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Slf4j
@Component
public class JdbcCompletionNotificationListener extends JobExecutionListenerSupport {

    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;

    private static final String SELECT_QUERY = "select fName,lName from Person";

    public JdbcCompletionNotificationListener(JdbcTemplate jdbcTemplate, @Qualifier("testDataSource") DataSource testDataSource) {
        this.jdbcTemplate = jdbcTemplate;
        this.dataSource = testDataSource;
        this.jdbcTemplate.setDataSource(this.dataSource);
    }

    @Value("${spring.application.name}")
    private String applicationName;

    @Override
    public void afterJob(JobExecution jobExecution) {

        log.info("After Job Execution check ....{}", applicationName);

        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Job finish ....");
            jdbcTemplate.query(SELECT_QUERY, (rs, row) -> new Person()
                    .builder()
                    .fName(rs.getString(1))
                    .lName(rs.getString(2))
                    .build())
                    .forEach(person -> {
                        log.info("Found person {}", person);
                    });
        }
    }

    @Override
    public void beforeJob(JobExecution jobExecution) {
        log.info("Before Job Execution check ....");
        int noOfRecords = jdbcTemplate.query(SELECT_QUERY, (rs, row) -> new Person()
                .builder()
                .fName(rs.getString(1))
                .lName(rs.getString(2))
                .build())
                .size();
        log.info("No of records found {}", noOfRecords);
    }
}
