package com.amdocs.batch.simple.reader;

import com.amdocs.batch.simple.model.Person;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

@Configuration
public class PersonFileReader {

    private static final String READER_NAME = "person-file-reader";

    @Bean
    public FlatFileItemReader reader() {
        return new FlatFileItemReaderBuilder<Person>()
                .name(READER_NAME)
                .resource(new ClassPathResource("simple-person.csv"))
                .delimited()
                .names(new String[]{"fName", "lName"})
                .fieldSetMapper(new BeanWrapperFieldSetMapper<Person>() {{
                    setTargetType(Person.class);
                }})
                .build();
    }
}
