package com.amdocs.batch.simple;

import com.amdocs.batch.simple.config.PersonTaskConfigurer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(value = PersonTaskConfigurer.class)
public class SimpleSpringBatchApplication {
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = new SpringApplicationBuilder(SimpleSpringBatchApplication.class)
				.web(WebApplicationType.NONE)
				.run(args);
	}

}
