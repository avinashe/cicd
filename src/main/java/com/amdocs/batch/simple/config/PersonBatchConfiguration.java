package com.amdocs.batch.simple.config;

import com.amdocs.batch.simple.listener.JdbcCompletionNotificationListener;
import com.amdocs.batch.simple.model.Person;
import com.amdocs.batch.simple.processor.PersonItemProcessor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableBatchProcessing
@EnableTask
//@EnableScheduling
public class PersonBatchConfiguration {

    private static final String JOB_NAME = "simple-person-job";
    private static final String STEP_NAME = "simple-person-step";

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;


    @Bean
    public Job job(JdbcCompletionNotificationListener listener, Step step) {
        return jobBuilderFactory.get(JOB_NAME)
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step)
                .end()
                .build();

    }

    @Bean
    public Step step(FlatFileItemReader reader, PersonItemProcessor processor, JdbcBatchItemWriter<Person> writer) {
        return stepBuilderFactory.get(STEP_NAME)
                .<Person, Person>chunk(5)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }
}
