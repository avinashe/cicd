package com.amdocs.batch.simple.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.task.configuration.DefaultTaskConfigurer;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class PersonTaskConfigurer extends DefaultTaskConfigurer {

    @Autowired
    public PersonTaskConfigurer(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource);
    }
}

