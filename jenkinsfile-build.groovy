pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                sh 'mvn --version'
            }
        }
        stage('Test') {
            steps {
                sh 'mvn test'
            }
        }
        stage('Deploy') {
            steps {
                sh 'mvn install'
            }
        }
    }
    post {
            always {
                echo 'Build finished'
            }
            success {
                echo 'Successful build.'
            }
            unstable {
                echo 'Failed tests'
            }
            failure {
                echo 'Failed to build'
            }
        }
}